**Resultados dos das análises contidas no Artido: Análise e Abordagens Multiobjetivo para o Problema deEscalonamento de Pátio Operado por Guindastes**

Estes resultados foram obtidos após a execução dos algortitmos GLS+ILS e NSGA-2 descritos no artigo Análise e Abordagens Multiobjetivo para o Problema de Escalonamento de Pátio Operado por Guindastes. A duração dos testes foi de 15 minutos para cada instância e com as sementes 0, 1, 2, 3, 4 e 5.

---

## Estrutura dos Arquivos

Os resultados estão em arquivos CSV nomeados de acordo com o algoritmo utilizado e a semente do teste. Em cada arquivo estão separados Instance (nome da instância utilizada); Objetive (valor da soma das funções objetivos com pesos iguais a 0.5); BAP cost (valor do primeiro objetivo); CSP cost
(valor do segundo objetivo)

---
